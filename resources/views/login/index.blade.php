 

<!DOCTYPE html>
<html>
        <head>


        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>


                <!-- Font Awesome CSS-->
                <!-- <link rel="stylesheet" href="vendor/font-awesome/css/font-awesome.min.css"> -->
                <link href="{{ asset('vendor/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet"> 

                <!-- Fontastic Custom icon font-->
                <link rel="stylesheet" href="css/fontastic.css">

                <!-- Google fonts - Roboto -->
                <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700">

                <!-- jQuery Circle-->
                <link rel="stylesheet" href="css/grasp_mobile_progress_circle-1.0.0.min.css">

                <!-- Custom Scrollbar-->
                <link rel="stylesheet" href="vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css">

                <!-- theme stylesheet-->
                <link rel="stylesheet" href="css/style.default.css" id="theme-stylesheet">

                <!-- Custom stylesheet - for your changes-->
                <link rel="stylesheet" href="css/custom.css">

                <!-- Favicon-->
                <link rel="shortcut icon" href="img/favicon.ico">
 
        </head>
        <body style="background: #f5f5f5;">
        
       <br>
       <br>
                <div class="container ">
                        <div class="row">
                                 
                                <div class="col-12 col-sm-10 col-md-8 col-lg-7 col-xl-7 mx-auto text-center card p-5">
                                                 
                                                 
                                        <div class="logo text-uppercase">
                                                <span      style="color:#ccc; font-size: 2em;font-weight: 700;">MMS</span> <strong class="text-primary" style=" font-size: 2em;font-weight: 500;">Login</strong>
                                                
                                        </div>
                                        <p  style="color:#ccc; font-size: 1em;font-weight: 600;">Mapping Management System</p> 
                                    
                                        <form method="get" class="text-left form-validate   p-3">
                                                <div class="form-group-material" style="color:##aaa;">
                                                        <input id="login-username" type="text" name="loginUsername" required data-msg="Please enter your username" placeholder="Username" class="input-material">
                                                        
                                                </div>
                                                <div class="form-group-material">
                                                        <input id="login-password" type="password" name="loginPassword" required data-msg="Please enter your password" placeholder="Password" class="input-material">
                                                        
                                                </div>
                                                <div class="form-group text-center"><a id="login" href="index.html" class="btn btn-primary">Login</a>
                                                        <!-- This should be submit button but I replaced it with <a> for demo purposes-->
                                                </div>
                                        </form>
                                        <a href="#" class="forgot-pass">Forgot Password?</a>
                                        <small>Do not have an account? 
                                        <a href="register.html" class="signup">Signup</a></small>
                                        
                         
                                 
                                </div>
                        </div>
                </div>
        <!-- JavaScript files-->
        <script src="vendor/jquery/jquery.min.js"></script>
        <script src="vendor/popper.js/umd/popper.min.js"> </script>
        <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
        <script src="js/grasp_mobile_progress_circle-1.0.0.min.js"></script>
        <script src="vendor/jquery.cookie/jquery.cookie.js"> </script>
        <script src="vendor/chart.js/Chart.min.js"></script>
        <script src="vendor/jquery-validation/jquery.validate.min.js"></script>
        <script src="vendor/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
        <!-- Main File-->
        <script src="js/front.js"></script>
  </body>
</html>
        












         


  
