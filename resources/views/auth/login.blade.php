@extends('layouts.app')

@section('content')
<div class="container " >
    <div class="row justify-content-center text-center" >


        <div class="col-12 col-sm-11 col-md-10 col-lg-9 col-xl-8">
            <div class="card m-5">
                <div class=" p-4">
                
                    <div class="logo text-uppercase">
                            <span      style="color:#ccc; font-size: 2em;font-weight: 700;">MMS</span> 
                            <span      style="color:#33b35a; font-size: 2em;font-weight: 700;">LOGIN</span> 
                            
                    </div>
                    <p  style="color:#ccc; font-size: 1.3em;font-weight: 900;">Mapping Management System</p> 
                </div>
                <!-- <div class="card-body"> -->
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row form-group-material">
                            
                            <div class="col-12 col-sm-11 col-md-10 col-lg-9 col-xl-8  mx-auto">
                                <input id="email" type="email" class=" input-material form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="   {{ __('E-Mail Address') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                            
                        </div>

                        <div class="form-group row">
                           
                            <div class="col-12 col-sm-11 col-md-10 col-lg-9 col-xl-8  mx-auto">
                                <input id="password" type="password" class=" input-material form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="   {{ __('Password') }}" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        

                        <div class="form-group row" style="color:#ccc;">
                            <div class="col-md-12 text-center">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group ">
                            <div class="col-md-12  ">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>
                            </div>
                        </div>
                        
                    </form>

                    <div class="form-group  ">
                            <div class="col-md-12 text-center ">
                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                    
                                        {{ __('Forgot Your Password?') }}
                                        <br>
                                    </a>
                                @endif
                                <br>
                                @guest
                                        
                                    @if (Route::has('register'))
                                        <a class="btn btn-link" href="/register" >     
                                            {{ __('Register') }} 
                                        </a>
                                    @endif
                                    
                                @endguest
                            </div>
                        </div>


                <!-- </div> -->
            </div>
        </div>
    </div>
</div>
@endsection
 