<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <title> Proyecto - @yield('title')  </title>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>

        <!-- <link href="{{ asset('css/miEstilo.css') }}" rel="stylesheet"> -->

    </head>
    <body>
        
        <br>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    @include('components/navBar')
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    @yield('content')
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    @include('components/footer')
                </div>
            </div>
        </div>
        

    </body>
</html>
