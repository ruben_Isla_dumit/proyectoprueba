<?php

use Illuminate\Database\Seeder;

class PaisesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('paises')->insert(
                                [
                                    'name' => 'Venezuela',
                                    'city' => 'Caracas',
                                    'name' => 'Brasil',
                                    'city' => 'Sao Paolo',
                                    'name' => 'Chile',
                                    'city' => 'Santiago ',
                                ]
        );
    }
}
