<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//         '/mi_url_personalizada/parametro', 'controlador@metodo'








// #########################--ADMINISTRACIÓN DE USUARIOS--#####################
Route::get  ('/usuarios'                        , 'UserController@index');   
Route::get  ('/usuarios/editar/{id}'            , 'UserController@edit');   
// #########################--ADMINISTRACIÓN DE USUARIOS--#####################



// #########################--ADMINISTRACIÓN DE PAISES--########################
Route::get  ('/paises'                          , 'CountriesController@index');   
Route::get  ('/paises/new/'                     , 'CountriesController@new');
// Route::get ('/paises/create'                   , 'PaisesController@create');
Route::post ('/paises/create'                   , 'CountriesController@create');
// #########################--ADMINISTRACIÓN DE PAISES--#########################



// #########################--RUTAS DEL SISTEMA--###############################
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
// #########################--RUTAS DEL SISTEMA--###############################